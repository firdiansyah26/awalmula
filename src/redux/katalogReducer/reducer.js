import {
    HANDLE_STATE, GET_LIST_KATALOG_SUCCESS, SET_LOADER
} from '../actionTypes'

const initState = {
    loader: false,
    listKatalog: [],
    form: {
        media_gallery_entries: []
    }
}

const katalogReducer = (state = initState, action) => {
    switch (action.type) {
        case SET_LOADER:
            return {
                ...state, loader: action.value
            }
        case HANDLE_STATE:
            return {
                ...state, [action.field]: action.value
            }
        case GET_LIST_KATALOG_SUCCESS:
            return {
                ...state, listKatalog: action.value
            }
        default:
            return state
    }
}

export default katalogReducer