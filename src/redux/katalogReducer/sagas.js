import * as actionType from '../actionTypes'
import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as config from '../../config/config'
import { setLoader } from './actions'
import dataJson from '../../config/data.json'

export function* getListKatalog(action) {
    try {
        yield put(setLoader(true))
        let _response = yield call(config.GET, config.URL.LIST_API_PRODUCT + "?searchCriteria[pageSize]=10")
        // let _response = { data: dataJson }

        let _data = _response.data.items

        _data.map(obj => {
            obj['descriptionKatalog'] = []

            obj.custom_attributes.map(obj1 => {
                if (obj1.attribute_code.includes('description') || obj1.attribute_code.includes('informasi_gizi') || obj1.attribute_code.includes('manfaat_produk')) {
                    obj1['title'] = capitalizeFirstLetter(obj1.attribute_code.split("_").join(" "))
                    obj['descriptionKatalog'].push(obj1)
                }
            })
        })

        yield put({ type: actionType.GET_LIST_KATALOG_SUCCESS, value: _data ? _data : [] })
        yield put(setLoader(false))
    } catch (error) {
        console.log('error : ', error)
    }
}

export default function* rootSaga() {
    yield all([
        takeLatest(actionType.GET_LIST_KATALOG, getListKatalog),
    ])
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}