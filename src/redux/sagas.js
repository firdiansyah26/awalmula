import { all } from 'redux-saga/effects';

import KatalogSagas from "../redux/katalogReducer/sagas"

export default function* rootSaga(getState) {
    yield all([
        KatalogSagas()
    ]);
}