import axios from 'axios';

const BASE_URL = 'https://staging.awalmula.co.id'
const HEADER = {
    'Content-Type': 'application/json'
}

export const URL = {
    BASE_URL: BASE_URL,
    LIST_API_PRODUCT: "/rest/default/V1/products",
    IMAGE: "/pub/media/catalog/product/"
}

// GLOBAL API METHOE
export function GET(url) {
    return axios.get(BASE_URL + url, HEADER)
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        })
}

export function formatRupiah(angka, prefix) {

    angka = angka.toString()

    let number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi),
        separator;

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix === undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}