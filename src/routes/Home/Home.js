import React, { useEffect, useState } from 'react'
import { useCallback } from 'react'
import BlockUi from 'react-block-ui';
import { Row, Col, Card, Modal, Tabs, Image, Button } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { getDataKatalog, handleState } from "../../redux/katalogReducer/actions";
import * as config from '../../config/config'
import parserHTML from 'html-react-parser'

const { Meta } = Card;
const { TabPane } = Tabs;

const Home = () => {
    let dispatch = useDispatch();
    const [colBlock, setColBlock] = useState(6);
    const [isModalVisible, setIsModalVisible] = useState(false);

    let katalogState = useSelector(state => state.KatalogReducer);
    let formState = katalogState.form

    let onResize = useCallback(() => {
        // let IH = window.innerHeight;
        let IW = window.innerWidth;

        if (IW > 1140) setColBlock(6)
        else if (IW > 840 && IW < 1139) setColBlock(8)
        else if (IW > 560 && IW < 839) setColBlock(12)
        else setColBlock(24)
    })

    let showModal = useCallback((data) => {
        dispatch(handleState('form', data))
        setIsModalVisible(true);
    }, [dispatch]);

    let handleCancel = useCallback(() => {
        setIsModalVisible(false)
    }, [setIsModalVisible])

    useEffect(() => {
        dispatch(getDataKatalog())
        onResize()
    }, [dispatch])

    window.onresize = onResize

    return (
        <React.Fragment>
            <BlockUi tag="div" blocking={katalogState.loading}>
                <main>
                    <div className="content">
                        <Row gutter={16}>
                            {
                                katalogState.listKatalog.map((obj, idx) => {
                                    return (
                                        <Col key={idx} span={colBlock}>
                                            <Card
                                                onClick={() => showModal(obj)}
                                                className='card-radius text-align-left'
                                                hoverable
                                                style={{ width: 240 }}
                                                cover={<img alt={obj.name} src={config.URL.BASE_URL + config.URL.IMAGE + obj.media_gallery_entries[0].file} />}
                                            >
                                                <Meta title={obj.name}
                                                    description={config.formatRupiah(obj.price, 'Rp. ')} />
                                            </Card>
                                        </Col>
                                    )
                                })
                            }
                        </Row>
                    </div>

                    {/* modal */}
                    {
                        formState ?
                            <Modal title={formState.name} visible={isModalVisible} onCancel={handleCancel} cancelText={'Back'}
                                footer={[
                                    <Button key="back" onClick={handleCancel}>
                                        Return
                                </Button>
                                ]}>
                                <Row gutter={16}>
                                    <Col className="gutter-row" span={24}>
                                        {
                                            formState.media_gallery_entries.map(obj => {
                                                return (
                                                    <Image
                                                        width={100}
                                                        src={config.URL.BASE_URL + config.URL.IMAGE + obj.file}
                                                    />
                                                )
                                            })
                                        }
                                    </Col>
                                </Row>
                                <Row gutter={16}>
                                    <Col className="gutter-row" span={24}>
                                        <Tabs defaultActiveKey="0">
                                            {
                                                formState.descriptionKatalog ?
                                                    formState.descriptionKatalog.map((obj, idx) => {
                                                        return (
                                                            <TabPane tab={obj.title} key={idx}>
                                                                {parserHTML(obj.value)}
                                                            </TabPane>
                                                        )
                                                    })
                                                    : ''
                                            }
                                        </Tabs>
                                    </Col>
                                </Row>
                            </Modal>
                            : null
                    }
                </main>
            </BlockUi>
        </React.Fragment >
    )
}

export default Home